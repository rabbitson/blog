module.exports = {
  title: 'Rabbitson의 공부방',
  base: "/blog/",
  description: 'Rabbitson&#39;s blog',
  dest: "public",
  themeConfig: {
    smoothScroll: true,
    sidebar: [
      {
        title: 'Posts',
        collapsable: true,
        sidebarDepth: 3, 
        children: [ 
          {
            title: '컴퓨터 알고리즘',
            children:[
              {
                title: '컴퓨터 알고리즘 성능분석 (1)',
                path: '/posts/algorigm.md',
              }
            ]
          }
        ]
      },
      {
        title: 'About',
        path: '/',
        collapsable: false,
        sidebarDepth: 1
      },
      {
        title: 'GitLap',
        path: 'https://gitlab.com/rabbitson/rabbitson',
        collapsable: false,
        sidebarDepth: 1
      }
    ]
  }
}
