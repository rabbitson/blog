---
title: Posts
meta:
  - name: description
    content: Page description
  - name: keywords
    content: js vuejs vuepress
---

# 컴퓨터 알고리즘 성능분석 (1)

> 컴퓨터 알고리즘 성능분석

##### 티아카데미 유튜브 방송을 통해 공부하였습니다. [여기](https://www.youtube.com/watch?v=Cc-YlbLOaqY&list=PL9mhQYIlKEhcqOXxPOhs6pNpq681RDK4J)
<!-- <p align="center">
  <img src="./images/thumbnail-256x256.png" />
</p> -->

<!-- [[toc]] -->

<!-- ##### Check out for more Markdown Extensions [here](https://vuepress.vuejs.org/guide/markdown.html#header-anchors) -->
## 컴퓨터 알고리즘

::: tip 컴퓨터 알고리즘의 정의
문제를 해결하기 위한 과정을 상세하게 단계적으로 표현한 것으로 입력과 출력으로 문제를 정의
:::
| 단계        | 내용            |
| ------------- |:-------------:| 
| 문제정의   | 현실 세계의 문제를 컴퓨터를 이용하여 풀 수 있도록 입력과 출력의 형태로 정의 |
| 알고리즘 설명      | 문제를 해결하기 위한 단계를 차례대로 설명     |
| 정확성 증명 | 항상 올바른 답을 내고 정상적으로 종료되는지 증명      |
| 성능분석 | 수행시간이나 사용공간에 대한 알고리즘의 성능을 비교하기 위한 분석 |

## 컴퓨터 알고리즘 성능분석

::: danger 특정 기계에서 수행 시간을 측정하는 방법
현실적으로 불가능
:::

::: danger 절대적 시간을 측정하여 비교하는 방법
공정한 비교가 불가능
:::

그리하여 점근적 표기법 사용으로 성능분석한다.

O-notation ( 빅오 표기 )

Ω-notation ( 오메가 표기 )

θ-notation ( 쎄타 표기 )
## 컴퓨터 알고리즘 성능평가

문제를 해결하는 알고리즘은 여러 종류가 있을 수 있는데 이중에서 가장 적합한 것을 고르기 위해서는 성능평가와 비교가 필요하며 이를 위해서 상대적 평가를 할 수 있는 점근적 표현법이 사용됨

::: tip 대체법(Substitution Method)
재귀알고리즘은 T(n)으로 간단히 계산하기가 쉽지 않기 때문에 추측을 통해 대체법을 이용하여 수학적 귀납법을 증명하는 방법을 이용함
:::

<!-- ::: details Click me to view the code
```js
console.log('Hello, VuePress!')
```
::: -->
